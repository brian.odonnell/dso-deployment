# DSO Course Deployment Configuration Example

This configuration is used by ArgoCD (https://argoproj.github.io/argo-cd/) to deploy our course applications.  We are also using Kustomize (https://github.com/kubernetes-sigs/kustomize) to override our container image tag.  

The pipeline template's deploy and release stages will automatically write the newest container tag, or release tag into the `kustomization.yaml` configuration for `stage` and `prod` overlays.

## Deployment Exercise

* Fork this repo
* Two options
    * clone locally to make changes
    * use Gitpod to make changes
* Copy the `flask-helloworld` folder in this repo and rename it to `flask-helloworld-<your student number>`
    * If you use Gitpod, use the `duplicate` command on the `flask-helloworld` folder
* Within that folder, replace all instances of `flask-helloworld-NN` with `flask-helloworld-<your student number>`
* Commit the changes to your fork
* Create a MR back to the source repository and add the course instructors as reviewers

